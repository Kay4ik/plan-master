import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';

class LoginScreen extends StatefulWidget {
  LoginScreen({Key key}) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.grey[900],
        body: Stack(
          children: <Widget>[
            Positioned.fill(
                child: Image.asset(
              "assets/images/login-ph.jpg",
              fit: BoxFit.cover,
            )),
            Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    width: MediaQuery.of(context).size.width * 0.9,
                    padding: EdgeInsets.symmetric(vertical: 8),
                    decoration: BoxDecoration(
                      color: Colors.grey[200].withOpacity(0.9),
                      borderRadius: BorderRadius.circular(12)
                    ),
                    child: Column(
                      children: <Widget>[
                        SizedBox(
                          height: 50,
                          child: TextField(
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              prefixIcon: Icon(EvaIcons.emailOutline),
                              hintText: "E-Mail Adresse"
                            ),
                          ),
                        ),
                        Divider(color: Colors.black,),
                        SizedBox(
                          height: 50,
                          child: TextField(
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              prefixIcon: Icon(EvaIcons.lockOutline),
                              hintText: "Passwort"
                            ),
                          ),
                        ),
                        Divider(color: Colors.black),
                        SizedBox(
                          height: 50,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: <Widget>[
                              FlatButton.icon(
                                icon: Icon(EvaIcons.loaderOutline),
                                label: Text("Konto erstellen"),
                                onPressed: (){},
                              ),
                              FlatButton.icon(
                                textColor: Colors.red,
                                icon: Icon(EvaIcons.arrowIosForward),
                                label: Text("Anmelden"),
                                onPressed: (){},
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                  SizedBox(height: 8),
                  Container(
                    width: MediaQuery.of(context).size.width * 0.9,
                    height: 50,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(12),
                      color: Colors.red.withOpacity(0.9)
                    ),
                    child: FlatButton.icon(
                      icon: Icon(EvaIcons.google),
                      label: Text("Mit Google fortfahren"),
                      textColor: Colors.white,
                      onPressed: () {},
                    )
                  ),
                ],
              ),
            ),
          ],
        ));
  }
}
