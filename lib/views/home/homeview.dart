import 'package:equinox/equinox.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:planmaster/blocs/navigation/bloc.dart';

class Homepage extends StatefulWidget {
  Homepage({Key key}) : super(key: key);

  @override
  _HomepageState createState() => _HomepageState();
}

class _HomepageState extends State<Homepage> {
  NavigationBloc _bloc;

  @override
  void initState() {
    _bloc = NavigationBloc();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<NavigationBloc, NavigationState>(
      bloc: _bloc,
      builder: (context, state) {
        return EqLayout(
          appBar: EqAppBar(
            title: state.title,
          ),
          bottomTabBar: EqTabBar.bottom(
            defaultSelected: state.index,
            onSelect: _bloc.navigate,
            tabs: <EqTabData>[
              EqTabData(icon: (_) => EqIcon(EvaIcons.gridOutline)),
              EqTabData(icon: (_) => EqIcon(EvaIcons.calendarOutline)),
              EqTabData(icon: (_) => EqIcon(EvaIcons.mapOutline)),
              EqTabData(icon: (_) => EqIcon(EvaIcons.barChart)),
              EqTabData(icon: (_) => EqIcon(EvaIcons.settings2Outline)),
            ],
          ),
          child: state.widget,
        );
      },
    );
  }

  @override
  void dispose() {
    _bloc.close();
    super.dispose();
  }
}
