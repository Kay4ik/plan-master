import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class SplashScreen extends StatelessWidget {
  const SplashScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Column(children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Icon(
                  EvaIcons.chevronLeft,
                  size: 70,
                  color: Colors.red,
                ),
                Column(
                  children: <Widget>[
                    Text(
                      "Kay Boschmann",
                      style: Theme.of(context).textTheme.display1,
                    ),
                  ],
                ),
                Icon(
                  EvaIcons.chevronRight,
                  size: 70,
                  color: Colors.red,
                ),
              ],
            ),
            Text(
              "Mobile Application Solutions",
              style: Theme.of(context).textTheme.subtitle.merge(TextStyle(color: Colors.red[200])),
            )
          ]),
          SizedBox(height: 100),
          SpinKitChasingDots(
            color: Colors.red,
          )
        ],
      ),
    );
  }
}
