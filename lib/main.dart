import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:planmaster/blocs/authentication/bloc.dart';
import 'package:planmaster/blocs/delegates/SimpleBlocDelegate.dart';
import 'package:planmaster/services/userRepository.dart';

void main() {
  BlocSupervisor.delegate = AuthenticateBlocDelegate();
  runApp(App());
}

class App extends StatefulWidget {
  App({Key key}) : super(key: key);

  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> {
  AuthenticationBloc _bloc;

  @override
  void initState() { 
    _bloc = AuthenticationBloc(userRepository: UserRepository());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<AuthenticationBloc>(
      builder: (_) => _bloc,
      child: MaterialApp(
        title: 'Plan Master', 
        theme: ThemeData(
          brightness: Brightness.light,
          accentColor: Colors.red,
        ),
        darkTheme: ThemeData(
          brightness: Brightness.dark,
          accentColor: Colors.red,
        ),
        home: BlocBuilder<AuthenticationBloc, AuthenticationState>(
          bloc: _bloc,
          builder: (context, state) {
            return state.widget;
          },
        ),
      ),
    );
  }

  @override
  void dispose() {
    _bloc.close();
    super.dispose();
  }
}