import 'package:flutter/material.dart';
import 'package:planmaster/views/home/homeview.dart';
import 'package:planmaster/views/login/loginScreen.dart';
import 'package:planmaster/views/splash/splashScreen.dart';

abstract class AuthenticationState {
  Widget get widget => Container();
}

class AuthUninitialized extends AuthenticationState {
  @override
  Widget get widget => LoginScreen();
}

class AuthUnauthenticated extends AuthenticationState {}

class AuthAuthenticated extends AuthenticationState {
  @override
  Widget get widget => Homepage();
}

class AuthLoading extends AuthenticationState {}
