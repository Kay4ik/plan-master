import 'package:meta/meta.dart';

@immutable
abstract class NavigationEvent {
  final int index;
  NavigationEvent(this.index);
}

class NavigateEvent extends NavigationEvent {
  NavigateEvent(int index) : super(index);
}