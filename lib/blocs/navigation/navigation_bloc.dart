import 'dart:async';
import 'package:bloc/bloc.dart';
import './bloc.dart';

class NavigationBloc extends Bloc<NavigationEvent, NavigationState> {
  @override
  NavigationState get initialState => InitialNavigationState();

  @override
  Stream<NavigationState> mapEventToState(
    NavigationEvent event,
  ) async* {
    switch (event.index) {
      case 1:
        yield CalendarNavigationState();
        break;
      case 2:
        yield MapNavigationState();
        break;
      case 3:
        yield StatsNavigationState();
        break;
      case 4:
        yield SettingsNavigationState();
        break;
      default:
        yield this.initialState; // index 0
        break;
    }
  }

  void navigate(int i) {
    this.add(NavigateEvent(i));
  }
}
