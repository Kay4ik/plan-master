import 'package:flutter/widgets.dart';
import 'package:meta/meta.dart';

@immutable
abstract class NavigationState {
  String get title => "Plan Master";
  int get index => null;
  Widget get widget => Container();
}
  
class InitialNavigationState extends NavigationState {
  @override
  String get title => "Übersicht";

  @override
  int get index => 0;
}

class CalendarNavigationState extends NavigationState {
  @override
  String get title => "Kalendar";

  @override
  int get index => 1;
}

class MapNavigationState extends NavigationState {
  @override
  String get title => "Karte";

  @override
  int get index => 1;
}

class StatsNavigationState extends NavigationState {
  @override
  String get title => "Statistik";

  @override
  int get index => 1;
}

class SettingsNavigationState extends NavigationState {
  @override
  String get title => "Einstellungen";

  @override
  int get index => 1;
}